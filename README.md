# My-Practice-Problems
Some problems and codes for Java practice.

The project contains some problems and codes I did for practice and exams.

Some of the problems you'll find inside are:

- The directory problem
Please see the attached file Directory Question.jpg for the question.

- Gradebook
Gradebook.java finds the average number of student with a particular letter grade.

- Matchstick problem
The problem asks to find the number of iterations it will take to cut down all the match sticks with length given, if we cut in iterations where we choose the smallest stick and cut its length from the rest.

- Nodelist Problem
There is a class node and a class which symbolises a list of nodes.
We want to dynamically sort the list of nodes such that if we print the node list after putting data into it, we get a sorted list always. The user of the node list should not make a explicit call to sort the node list.
 
- Relative positioning problem
Please see attached file 'Relative positioning problem.png' for the question.

- Two Sum
Given an array of integers, return indices of the two numbers such that they add up to a specific target. You may assume that each input would have exactly one solution.

- Graph - Dijkstra's Algorithm

- Graph - BFS and DFS traversals

- MergeSort

- QuickSort

- Tree traversal: Preorder, inorder, postorder